---
layout: markdown_page
title: "Category Vision - Error Tracking"
---

- TOC
{:toc}

## Error Tracking

| | |
| --- | --- |
| Stage | [Monitor](/direction/monitor/) |
| Maturity | [Minimal](/direction/maturity/) |

### Introduction and how you can help
Thanks for visiting this category strategy page on Error Tracking in GitLab. This page belongs to the [Health](/handbook/engineering/development/ops/monitor/health/) group of the Monitor stage and is maintained by Sarah Waldner ([swaldner@gitlab.com](mailto:<swaldner@gitlab.com>)).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AError%20Tracking) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AError%20Tracking) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for Error Tracking, we'd especially love to hear from you.

### Overview
Error Tracking is the process of proactively identifying application errors and fixing them as quickly as possible. At GitLab, this functionality is based on an integration with [Sentry](sentry.io) which aggregates errors found by Sentry in the GitLab UI and provides the tools to triage and respond to the critical ones. GitLab leverages Sentry intelligence to provide pertinent information such as the user impact or commit that caused the bug. Throughout the triage process, Users have the option of creating GitLab issues on critical errors to track the work required to fix them, all without leaving GitLab. Our mission is to reduce time spent fixing errors by enabling triage, response, and resolution in a single tool: GitLab.

<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->
#### Target Audience
The primary persona for Error Tracking is the [Software Developer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer). Tracking and triaging errors is a part of any release process and the most qualified individual to solve the error is the developer who wrote the code. Secondarily, we are building Error Tracking for [DevOps Engineers](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer) who leverage it while investigating larger scale incidents or outages.
<!--
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
### Challenges to address
The following list of Jobs to be done is an aggregation of the most common responsibilities we identified for Software Developers surrounding error management and response. These are the workflows we aim to streamline and improve. Please check out the [research](https://gitlab.com/gitlab-org/ux-research/issues/366) which validates these findings.

* Respond to customer support tickets
* Manually investigate error alerts
* Resolve errors in code
* Communicate status of problems to stakeholders


<!--
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->
### Where we are Headed
When our vision for Error Tracking is fully realized, Developers will know about errors before their customers report them - every time. Following a release or during daily triage activities, GitLab will it make simple to identify new errors and the customer impact. By intuitively surfacing errors on Merge Requests or Releases, we can ensure responsible individuals have an aggregated view of errors resulting from a deployment. GitLab's ownership of the development workflow makes it simple to remove additional interfaces from the tool chain which will reduce time spent:

* Triaging errors - Errors live in the same place as code and are immediately correlated to the commit or merge request that caused it
* Investigating errors - The stack trace is embedded in GitLab issues and users can drill into the specific line of code via hyperlinks
* Tracking work - Close the GitLab issue once a fix is verified to communicate with stakeholders that services have been restored

<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->
#### What's Next & Why
Our plan is to provide a streamlined triage experience where developers can easily identify new, important errors directly within GitLab and quickly roll out a fix. To mature Error Tracking from minimal to viable, we will be focusing on:

* Creating a [detailed view of individual errors in GitLab](https://gitlab.com/gitlab-org/gitlab/issues/32464)
* Adding [filtering and sorting capabilities to the error list view](https://gitlab.com/groups/gitlab-org/-/epics/2031)
* Correlating errors with [releases](https://gitlab.com/gitlab-org/gitlab/issues/33884), [merge requests](https://gitlab.com/gitlab-org/gitlab/issues/33886), and [commits](https://gitlab.com/gitlab-org/gitlab/issues/33885)
* [Opening GitLab issues from errors](https://gitlab.com/gitlab-org/gitlab/issues/33156)
* Making it quick to get started on Sentry by [deploying it to the cluster](https://gitlab.com/gitlab-org/gitlab/issues/26513) your application is running on in a few clicks
* [Automatically reporting releases to Sentry](https://gitlab.com/gitlab-org/gitlab/issues/26028) to reduce manual, repetitive tasks


<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->
#### What is not planned right now
For the immediate future, GitLab's Error Tracking offering will be based on an integration with [Sentry](sentry.io). We do not plan to build out error monitoring solely in GitLab.
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

#### Maturity Plan
This category is currently at the `minimal` maturity level, and our next maturity target is `viable` (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). All work scoped for `viable` is captured in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/1625).
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

### User Success Metrics
We will know we are on the right trajectory for Error Tracking when we are able to observe the following:
* Broad adoption across internal engineering teams (i.e. we are [dogfooding](https://about.gitlab.com/handbook/values/#dogfooding))
* Increase in instances/projects with [Sentry Error Tracking enabled](https://docs.gitlab.com/ee/user/project/operations/error_tracking.html#enabling-sentry)
* Increase in instances [generating issues from Sentry errors](https://gitlab.com/gitlab-org/gitlab/issues/33847)
* Increase in the number of [clusters with Sentry deployed](https://gitlab.com/gitlab-org/gitlab/issues/26513)

<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->
### Why is this important?
At a 30,000 ft view, adding Error Tracking to GitLab drives the Single app for the DevOps lifecycle vision and works to speed up the broader DevOps workflow. We can eliminate an additional interface from the multitude of tools Developers are required to use each day to do their jobs. Furthermore, GitLab has the opportunity to surface errors early and often in the development process. By surfacing errors caused by a particular commit, merge request, or release, we can easily prevent our user's customers from experiencing the bug at all.

We are confident in our execution of this vision as Sentry already provides correlation between errors and commits by identifying [suspect commits](https://docs.sentry.io/workflow/releases/?platform=browser#after-associating-commits). In just a few milestones, we will be able to take advantage of this intelligence and extend it within the GitLab interface by correlating errors and merge requests and releases.
<!--
- Why is GitLab building this feature?
- Why impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->
## Competitive Landscape

* [Raygun](https://raygun.com/)
* [Rollbar](https://www.google.com/aclk?sa=l&ai=DChcSEwjWtYX5qbjlAhUH22QKHVlqBoUYABAAGgJwag&sig=AOD64_0vLeLukSK6oYi8AuSM04Scz29t3Q&q=&ved=2ahUKEwijkP_4qbjlAhWJqp4KHVQeClQQ0Qx6BAgIEAE&adurl=)
* [Airbrake](https://airbrake.io/)

<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## Top Customer Success/Sales Issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## Top User Issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## Top Internal Customer Issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

## Top Strategy Item(s)
<!-- What's the most important thing to move your strategy forward?-->
