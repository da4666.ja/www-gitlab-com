---
layout: handbook-page-toc
title: "Marketing Programs Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Marketing Program Managers

Marketing Program Managers focus on executing, measuring and scaling GitLab's marketing campaigns, landing a message strategically focusing on a target audience using channels such as email nurture, digital ads, paid and organic social, events, and more. Marketing programs works with Content Marketing and Product Marketing to activate content in the most effective manner to drive leads for SDRs and Sales. Webcasts, gated content, and Pathfactory strategy/best practices are owned by the Digital Marketing Programs team.

For MPM team members, the [in-depth processes page contains the extremely detailed steps to execute on campaigns, gated content, events, and webcasts.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/processes-details/)

## Responsibilities

**Jackie Gragnola**  
*Manager, Marketing Programs*
* **Team Prioritization**: plan prioritization of campaigns, related content and webcasts, event support, and projects for the team
* **Hiring**: organize rolling hiring plan to scale with organization growth
* **Onboarding**: create smooth and effective onboarding experience for new team members to ramp quickly and take on responsibilities on the team
* **Transition of Responsibilities**: plan for and organize efficient handoff to new team members and between team members when prioritization changes occur
* **NORAM - West Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)
* **Corporate Worldwide Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Corporate Events and Alliances teams)

**Agnes Oetama**  
*Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **Virtual Events**: project management, set up, promotion, and follow up of all virtual events (webcasts, demos, virtual sponsorship)
* **Bi-weekly Newsletter**: coordinate with cross-functional teams on topics and set up newsletter in marketo
* **APAC Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)
* **APAC Corporate Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Corporate Events and Alliances teams)

**Jenny Tiemann**  
*Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **NORAM - Central & PubSec Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)
* **EMEA Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)
* **Ad-Hoc Emails**: coordination of copy, review, and set up of one-time emails (i.e. security alert emails, package/pricing changes)
* **Nurture Campaigns**: strategize and set up campaigns (email nurturing)

**Zack Badgley**  
*Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **NORAM - East Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)

**Sarah Daily**  
*Digital Marketing Programs Manager*
* **Email Review/Approve**: review and approve all emails (minus field event related emails) to adhere to the [email review protocol](/handbook/business-ops/resources/#email-review-protocol) and [brand guidelines](/handbook/marketing/corporate-marketing/#brand-guidelines)
* **Conversion Optimization**: A/B testing including gathering and analyizing data, formulating hypotheses, drafting copy, and reporting on performance
* **Data and Analysis**: gathering data and reporting on patterns in email marketing program to drive qualified traffic to website for specific campaigns, date ranges, or email types

**Marketing Operations**
* Cleaning and uploading of lead lists post-event
* Creation of geo target lists in SFDC for email promotion of events

*Each team member contributes to making day-to-day processes more efficient and effective, and will work with marketing operations as well as other relevant teams (including field marketing, content marketing, and product marketing) prior to modification of processes.*

### Order of assignment for execution tasks:
{:.no_toc}
1. Primary responsible MPM  
1. Secondary MPMs  (if primary MPM is OOO or not available)
1. Marketing OPS (if MPMs are OOO or not available)

### Holiday coverage for S1 security vulnerabilities email communication

In the event of an S1 (critical) security vulnerability email communication is needed during the holidays, please create an issue using *[Email-Request-mpm template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/Email-Request-mpm.md)* and ping in #marketing-programs tagging @jennyt @zbadgley @aoetama @jgragnola @jjcordz.

## Email marketing calendar

The calendar below documents the emails to be sent via Marketo and Mailchimp for:
1. event support (invitations, reminders, and follow ups)
1. ad hoc emails (security, etc.)
1. webcast emails (invitations, reminders, and follow ups)
1. milestones for nurture campaigns (i.e. when started, changed, etc. linking to more details)

*Note: emails in the future may be pushed out if timelines are not met for email testing, receiving lists from event organizers late, etc. The calendar will be updated if the email is pushed out. Please reference the MPM issue boards (described below on this page) to see progress of specific events/webcasts/etc.*

<figure>
  <iframe src="https://calendar.google.com/calendar/b/1/embed?showPrint=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=gitlab.com_bpjvmm7ertrrhmms3r7ojjrku0%40group.calendar.google.com&amp;color=%23B1365F&amp;ctz=America%2FLos_Angeles" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
</figure>

## Email nurture program

### Visualization of current nurture streams

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://www.lucidchart.com/documents/embeddedchart/7889a7fb-e1f0-4c67-92ae-7becd009625f" id="XA5ojeoO~Tej"></iframe></div>

### Top funnel nurture

The goal of the top funnel email nurture is to further engage early stage leads so they become MQLs and/or start a free trial. Currently the top funnel nurture is segmented into 3 different tracks : Cloud native, CI/CD and Reducing cycle times.

High level overview on the messaging for each track:

- Cloud Native: The messaging for this track is centered around benefits of cloud native transformation (Kubernetes, containers and the likes) and how GitLab can help. This track is suitable for those that consumed cloud native assets off our website/partner sites and those that attended cloud native themed offline/online events.
- CI/CD: The messaging for this track is centered around the benefits of GitLab's powerful built-in CI/CD, why this is better than a traditional plug-in CI/CD solution. This track is suitable for those that consumed CI/CD assets off our website/partner sites and those that attended offline/online CI/CD themed events.
- Reduce cycle time: The messaging for this track is centered around the benefits of reducing cycle time and how GitLab can help to accomplish that. This is the more generic catch-all track.

The top funnel emails are sent out on a weekly basis on Tuesdays at 7 a.m. local time.

*Note: A lead will be transferred to the stream related to the latest asset they downloaded. For example if lead A is already in the cloud native stream and they registered for the CI/CD webcast, they will move to the CI/CD stream and start receiving the first email in that stream.  Currently, this is the best way to align content to the prospect's most current topic of interest. The framework is subject change if we eventually categorize assets by high-intent vs. low intent or if we bring on a content insight and activation platform.*

**[>>  Email copies for TOFU nurture](https://docs.google.com/presentation/d/1EzIFyaJB5N_bZ6qmP89Y_h4IGRp048MPzQ5vqhmTNrM/edit#slide=id.g29a70c6c35_0_68)**

### SaaS trial nurture

SaaS Gold trial nurture communication are sent via Marketo and Outreach throughout the 30-day free trial period.

**Goal of the Marketo nurture:** Educate trialers on key features within GitLab Gold SaaS tier.

**Goal of SDR Outreach nurture:** Qualify and meetings setting for SaaS Gold trialers.

**[>> Email copies for SaaS Gold package trial nurture](https://gitlab.com/groups/gitlab-com/marketing/-/epics/98)**

### Self-hosted trial nurture

Self Hosted Ultimate trial nurture communication are sent via Marketo and Outreach throughout the 30-day free trial period.

**[>> Email copies for Self-hosted Ultimate package nurture](https://docs.google.com/presentation/d/1KSAZFwz3nvSTIXOP8urGWW6dJWhtpawVKFcaoFLDPdg/edit#slide=id.g2ae1ad1112_0_22)**

## Marketo vs. PathFactory

### Summary: Pathfactory ≠ Nurture. Pathfactory is a tool that - instead of driving to a single asset - drives to a related-content adventure.

Nurture is a channel to bring an individual to the content. Just like ads, social, website, etc. drive to CTAs, Pathfactory link is the CTA - a *powerful* CTA because it can lead the individual down a "choose your own adventure" path of consumption which we track.

### FAQ:

1. What is the Goal of PathFactory and Marketo?

>**Marketo nurture:** To keep the conversation going about GitLab (keep us top of mind).

>**Pathfactory:** To increase consumption/activation of GitLab content.

1. Do the lead lifecycle stage play into each nurture (For example, do we only want leads with status "Inquiry" to receive PathFactory nurture and leads with status "MQL" to only receive Marketo nurture?) ?

>No, both Marketo and Pathfactory can be leveraged to help push leads further down the funnel.

1. Can records be in Marketo nurture and PathFactory nurture at the same time? If not, is 1 prioritized over the other?

>Yes, the Pathfactory track acts as a supplement to the existing Marketo nurture instead of a replacement.

1. How does the post event follow up process look like? Specifically, when will it be appropriate to add leads to an existing Marketo nurture vs. just linking to a PathFactory track in the follow up email?

>1) **For event leads where there is no relevant Marketo nurture:** In the follow up email, attach a link to **a piece of content** in a relevant PathFactory track.

>2) **For event leads where a relevant Marketo nurture exists**: In the follow up email, attach a link to a piece of content in a relevant PathFactory track that is **not referenced in the Marketo nurture**. Additionally, if/when requested by the FMM team, we should also add the leads to the Marketo nurture to keep the conversation going.

# How we work together

## Issue management in GitLab

### How our team stays productive
Below are issue views that the Marketing Program Team monitors for in-progress and upcoming action items:
* [MPM Priority](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20Priority)
* [Landing Pages](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=✓&state=opened&label_name[]=MPM%20-%20Landing%20Page%20%26%20Design)
* [Invitations & Reminder Emails](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Invitations%20%26%20Reminder)
* [Follow Up Emails](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Follow%20Up%20Emails)
* [Add to Nurture](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Add%20to%20Nurture)
* [MPM - Radar](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Radar)

### Marketing Programs labels in GitLab

* **Marketing Programs**: General labels to track all issues related to Marketing Programs. This brings the issue into the board for actioning.
* **MPM - Radar**: Holding place for any issues that will need Marketing Program Manager support, including gated content, events, webcasts, etc.
* **MPM - Supporting Epic / Issue Created**: Indicates that the epic and supporting issues were created for the MPM - Radar issue. At the time this label is applied, the "MPM - Radar" label will be removed.
* **MPM - Secure presenters and schedule dry runs**: Used when MPM is securing presenters and Q&A support for an upcoming virtual event.
* **MPM - Landing Page & Design**: Used by Marketing Program Manager to indicate that the initiative is in the stage of landing page creation and requesting design assets from the web/design team.
* **MPM - Marketo Flows**: Used by Marketing Program Manager to indicate that the initiative is in the stage of editing/testing of flows in Marketo.
* **MPM - Create Target List**: Used by Marketing Program Manager requested of Marketing Ops and in collaboration with Field Marketing to receive a list curated for the geo target. Marketo smart lists for larger metro areas around the world are built to expedite list creation. Additional curation done in Salesforce.
* **MPM - Invitations & Reminder**: Used by Marketing Program Manager when the initiative is in the stage of identifying segmentation to target and outreach strategy.
* **MPM - Follow Up Emails**: Used by Marketing Program Manager when initiative is in the stage of writing and reviewing relevant emails (reminders, follow up, etc.).
* **MPM - Add to Nurture**: Used by Marketing Program Manager when initiative is in the stage of being added to nurture.
* **MPM - Project**: For non-campaign based optimizations, ideation, and projects of Marketing Program Managers
* **MPM - Blocked/Waiting**: Designates that the MPM is blocked by another team member from moving forward on the issue.
* **MPM - Switch to On-demand**: Used by Marketing Program Manager when switching webcast landing page and subsequent marketo programs to on-demand post event.
* **MPM Priority**: to be used by MPMs to organize their top priority tasks. These are not to be applied by other team members.

# Marketing Programs support requests

## Requesting to "Gate" a Piece of Content

This section has been updated and moved to the [gated-content page](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content)

### A visual of what happens when someone fills out a form on a landing page

![](/source/images/handbook/marketing/marketing-programs/landing-pages-flow-model.png)

❌ **A landing page with a form should never be created without the inclusion and testing by Marketing Programs and/or Marketing Ops.**

Please post in the [slack channel #marketing-programs](https://gitlab.slack.com/messages/CCWUCP4MS) if you have any questions.

## Requesting Virtual (Online) Events support

The table below shows execution tasks related to various types of Virtual Events along with the DRI for each task.

<div class="tg-wrap">
<table class="tg">
  <tr>
    <th class="tg-k6pi">Tasks</th>
    <th class="tg-k6pi">Due dates</th>
    <th class="tg-yw4l">GitLab hosted - end user webcast</th>
    <th class="tg-yw4l">Non GitLab hosted - end user webcast</th>
    <th class="tg-yw4l">Virtual sponsorship</th>
    <th class="tg-yw4l">Account specific webcast</th>
    <th class="tg-yw4l">Reseller enablement webcast</th>
    <th class="tg-yw4l">Community virtual event support</th>
  </tr>
  <tr>
    <td><strong> <a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/%20MPM-00-secure-presenters_dryruns.md">Secure presenters</a></strong></td>
    <td>-40 business days</td>
    <td>MPM or Requestor</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
    <td>Channel Sales Manager</td>
    <td>Community Program Manager</td>
  </tr>
  <tr>
    <td><strong>Host prep call with internal team</a></strong></td>
    <td>-35 business days</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>Only for large segment accounts: MPM</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?nav_source=navbar">Request design assets</a></strong></td>
    <td>-33 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
    <td>N/A</td>
    <td>Community Program Manager</td>
  </tr>
  <tr>
    <td><strong> <a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-02-landing-page.md"> Draft landing page copy </a></strong></td>
    <td>-33 business days</td>
    <td>Requestor or Presenter</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
    <td>Channel Sales Manager</td>
    <td>Community Program Manager</td>
  </tr>
  <tr>
    <td><strong> <a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-02-landing-page.md"> Create landing page to track registration </a> </strong></td>
    <td>-31 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>Optional:MPM</td>
  </tr>
  <tr>
    <td><strong> Organize Outreach master sequence creation </strong></td>
    <td>-30 business days</td>
    <td>Only if audience size is 1K+:MPM</td>
    <td>Only if audience size is 1K+:MPM</td>
    <td>Only if audience size is 1K+:MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
   <tr>
    <td><strong> Drive registration - create issue for blog post promotion </strong></td>
    <td>-30 business days</td>
    <td>Optional: Requestor</td>
    <td>Only if we have paid ads budget: Requestor</td>
    <td>Only if we have paid ads budget: Requestor</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>Community program manager</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03B-virtualevent-promotion.md"> Drive registration - Promote in bi-weekly newsletter </a> </strong></td>
    <td>-21 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>Community program manager</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/corporate-marketing/blob/master/.gitlab/issue_templates/social-request.md"> Drive registration - create issue for organic social promotion </a> </strong></td>
    <td>-21 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>Optional(to be discussed in prep call): MPM</td>
    <td>N/A</td>
    <td>Community program manager</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03-invitations-reminder.md"> Drive registration - Targeted email invite to GitLab database </a> </strong></td>
    <td>-16 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>MPM</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong> <a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03B-virtualevent-promotion.md"> Drive registration - Draft SDR email invite </a></strong></td>
    <td>-14 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>Optional (to be discusseed in prep call): MPM</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong> Create presentation slides </strong></td>
    <td>-7 business days</td>
    <td>Requestor or Presenter</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>Optional (to be discusseed in prep call): SALs</td>
    <td>Channel Sales Manager</td>
    <td>Community program manager</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-04-follow-up-email.md"> Draft copy for post event follow up emails </a></strong></td>
    <td>-5 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
    <td>Channel Sales Manager</td>
    <td>Community program manager</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/%20MPM-00-secure-presenters_dryruns.md">  Host dry run </a> </strong></td>
    <td>-5 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong> Host virtual event</strong></td>
    <td>0 business days</td>
    <td>MPM</td>
    <td>3rd party vendor</td>
    <td>3rd party vendor</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong> Moderate webcast</strong></td>
    <td>0 business days</td>
    <td>MPM or Presenter</td>
    <td>3rd party vendor</td>
    <td>N/A</td>
    <td>SALs</td>
    <td>Channel Sales Manager</td>
    <td>Community program manager</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-06-on-demand-switch.md"> Upload live webcast recording to youtube </a></strong></td>
    <td>0 business days</td>
    <td>MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (to be discussed in prep call): MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-06-on-demand-switch.md"> Convert registration page to on-demand </a> </strong></td>
    <td>+2 business days</td>
    <td>MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (to be discussed in prep call): MPM</td>
    <td>MPM</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/blob/master/.gitlab/issue_templates/event-clean-upload-list.md"> Ping MOPs to upload lead list to CRM (SFDC) </a></strong></td>
    <td>+1 business days</td>
    <td>N/A</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-04-follow-up-email.md"> Send marketo follow up emails </a></strong></td>
    <td>+1 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM or Community program manager</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-07-Retrospective.md"> Facilitate retrospective </a></strong></td>
    <td>+2 to +7 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
  </table>
</div>

### Below is a step by step guide on how to request MPM support for a virtual event.

**⚠ Note: Virtual Event requests should be submitted no less than 45 days before event date so the new request can be added into the responsible Marketing Program Manager's (MPM) workflow and to ensure we have enough leeway for promotion time.**

#### Step 1: Create a [virtual event request issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM_VirtualEvent_Request.md) in the digital marketing programs project.
*  Please use the `MPM_VirtualEvent_Request.md` issue template linked above
*  Please put the date of the virtual event as a due date
*  Please specify the type of virtual event
*  @ mention MPM in the issue comment to confirm requested date is feasible

#### Step 2: MPM will create the Virtual Event EPIC.

#### Step 3: MPM will create the necessary MPM support issues (linked in table above) and add to epic.

## Requesting Offline Event support

Below is an overview of the process for requesting support for a conference, field event, or owned event. Please slack #marketing-programs if you have any questions.

#### Step 1: FMM creates issue using template
* [FMM creates issue](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new) using *Event_Field_Marketing* template with automatic label of "status:plan"
* FMM assesses the event, decides go/no-go, and receives budget approval
* FMM documents at the top of the issue the elements (i.e. landing page, speaking session, etc.) needed from the MPM and changes status to "status:wip"

⚠️ *If the FMM does not list the elements needed of the MPM in the issue, the MPM will change the status back to "status:plan" until the necessary details are included. This will minimize back-and-forth with MPM and let them take action appropriately and efficiently.

#### Step 2: MPM creates the event epic
* When "status:wip" is on the issue and necessary elements are documented, MPM creates epic for the event.
* Naming convention: [Event Name] - [3-letter Month] [Date], [Year]
* MPM copy/pastes code below into the epic

```
## [Main Issue >>]()

## [Copy for landing page and emails >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## Event Details
  * `place details from the event issue here`
  * [ ] [main salesforce campaign]()
  * [ ] [main marketo program]()

## Issue creation

* [ ] Follow up email issue created - MPM
* [ ] List clean and upload issue created - MOps
* [ ] Landing page issue created - MPM
* [ ] Invitation and reminder issue created - MPM
* [ ] Add to nurture issue created - MPM
* [ ] Target segment issue created - MOps

cc @nlarue to create list upload issue

```

#### Step 3: MPM updates the event epic details
* In epic description, MPM will copy over the "need-to-know" event details section from the main event issue the FMM had created.
* In "Issue Creation" section, delete any pieces that aren't necessary based on the FMM answers for event elements at top of main event issue

#### Step 4: MPM and MOps create the necessary issues and add to epic

* Using the relevant issue templates MPMs and MOps create issues in proper project and then link from the epic.
  * [Landing page](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-02-landing-page) - due date T-
  * [Invitations & reminder](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-03-invitations-reminder)
  * [Follow up email](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email)
  * [Add to nurture](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-Add-to-Nurture)
* Naming convention: [Issue naming convention] - [Event Name]
* During these issue creations, DUE DATE is required to be added by issue creator.

⚠️ Note: MOps is required to create their list clean and upload issue, and target list issue in their project with the proper template and associate back to the epic.

☝️ *Tip: If you want a shortcut to determine due dates, use timelines / SLAs for events. Add the event as a new row, add the start and end date, and look at the resulting due dates for each piece.*

### Offline Events channel types

*[See full campaign progressions here](/handbook/business-ops/resources/#conference)*

* **Conference:** Any large event that corprate events is the DRI for, we have paid to sponsor, have a booth/presence at, and are sending representatives from GitLab.
  *  Note: this is considered an Offline Channel for bizible reporting because we do not host a registration page, and receive a list of booth visitors post-event.
  *  Example: DevOps Enterprise Summit, AWS re:Invent
* **Field Event:** An event that field marketing is the DRI for, we have paid to participate in but do not own the registration or event hosting duties.
  *  Note: this is considered an Offline Channel for bizible reporting because we do not host a registration page, and receive a list of attendees post-event.
  *  Hint! If we do not own the registration page for the event, but it is not a conference (i.e. a dinner or breakfast), it is likely a Field Event. Comment in the issue to Jackie if you have need help.
  *  Example: Lighthouse Roadshow (hosted by Rancher), All Day DevOps (virtual event hosted by )
* **Owned Event:** This is an event that we have created, own registration and arrange speaker/venue.
  *  Note: this is considered an Online Channel for bizible reporting because we manage the registration through our website.
  *  Example: GitLab Connect Atlanta, Gary Gruver Roadshow
* **Speaking Session:** This is a talk or speaking session at a conference or field event.
  *  Note: this is considered an Offline Channel for bizible reporting because we do not host a registration page, and receive a list of talk attendees post-event.
  *  Example: Sid & Priyanka's Talk at AWS re:Invent

## Adding to the /events page

For full instructions to add entry to the `/events` page, see the [Marketing Events page](/handbook/marketing/events/index.html#how-to-add-events-to-the-aboutgitlabcomevents-page). 



## Requesting an Email  

Process to request an email can be found in the [Business OPS](/handbook/business-ops/resources/#requesting-an-email) section of the handbook.   

Primary party responsible for various email types can be determined using the [table above](#responsibilities).   

## Requesting to add leads to the top funnel nurture

To add leads to the top funnel nurture, please reach out to one of the marketing program managers through an issue. The marketing program manager will assess the request and can add the leads to the proper stream via a 'Request Campaign' marketo flow step.

- Flow step to add leads to TOFU nurture 'Cloud Native' track -> 'request Smart Campaign: TOFU nurture.Add to stream 1: Cloud Native (trigger)'
- Flow step to add leads to TOFU nurture 'CI/CD' track -> 'request Smart Campaign: TOFU nurture.Add to stream 4: CI/CD (trigger)'
- Flow step to add leads to TOFU nurture 'Reduce Cycle Time (Generic DevOps)' track -> 'request Smart Campaign: TOFU nurture.Add to stream 2: Reduce Cycle Time_Forrester (trigger)'

## Requesting to add a new asset to the top funnel nurture

If you'd like to add an asset to the top funnel nurture, please create an issue in the [digital marketing programs](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?nav_source=navbar) project, explaining why you'd like the asset added and which stream you'd like it added to. Please assign the issue to @aoetama and ping her in the comment.

*Note: If the asset does not fit into one of the active streams, we will keep track of it as a wishlist and reference as input when we decide to roll out new streams in the future.*

# Programs reporting

MPM uses SFDC reports and dashboards to track program performance real-time. Data from the below SFDC reports/dashboards along with anecdoctal feedback gathered during program retros will be used as guidelines for developing and growing various marketing programs.

The SFDC report/dashboard is currently grouped by program types so MPMs can easily compare and identify top performing and under performing programs within the areas that they are responsible for.

#### [Webcast dashboard](https://gitlab.my.salesforce.com/01Z6100000079e6)

The webcast dashboard tracks all webcasts hosted on GitLab's internal webcast plaform. It is organized into 3 columns. The left and middle columns tracks 2 different webcast series (Release Radar vs. CI/CD webcast series). The right column tracks various one-off webcasts since Jan'18.

#### [Live Demo dashboard](https://gitlab.my.salesforce.com/01Z6100000079f4)

The LIVE Demo dashboard is organized into 2 columns. The left column tracks the bi-weekly Enterprise Edition product demos (1 hour duration). The bi-weekly Enterprise Edition product demos ran between Q1'18 - Q2'18.
The right column tracks the weekly high level product demo + Q&A session (30 minutes duration). The weekly high level product demo + Q&A session was launched in Q4'18 and currently running through the end of Feb 2019.

#### [Virtual Sponsorship dashboard](https://gitlab.my.salesforce.com/01Z61000000UD44)

### Key Metrics tracked in ALL virtual events dashboards

*Note: Virtual Events include Webcast, LIVE demos and Virtual Sponsorship*

**Total Registration :** The number of people that registered for the virtual event regardless whether they attend or not.

**Total Attendance:** The number of people that attended the LIVE virtual event (exclude people who watched the on-demand version).

**Attendance Rate:** % of people that attended the LIVE virtual event out of the total registered (i.e: Total Attendance / Total Registration).

**Net New Names:** The number of net new names added to our marketing database driven by the virtual event. Because a net new person record may be inserted into our CRM (SFDC) as a lead or a contact object therefore, we need to add `Total net new leads` and `Total net new contacts` to get the overall total net new names.

**Influenced Pipe:** Total New and Add-on business pipeline IACV$ influenced by people who attended the LIVE virtual event. The webcast and live demo dashboards currently use SFDC out of the box `Campaigns with Influenced opportunities` report type because Bizible was implemented in June'18 and therefore the attribution report did not capture data prior to this. We plan to migrate webcast and live demo influenced pipe reports to Bizible attribution report in the next dashboard iteration so they align with overall marketing reporting.

# Programs logistical set up

### Webcast

Webcast program set up can be found in the [Business OPS](/handbook/business-ops/resources/#logistical-setup) section of the handbook.

### Newsletter

#### Process for bi-weekly newsletter

Open an issue in the [Digital Marketing Programs project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/) using the `Newsletter` template, including the newsletter send date in the issue title.

Assign to `@aoetama` and a member of the editorial team, based on the following schedule:

- 8/25 - `@vsilverthorne`
- 9/10 - `@skassabian`
- 9/25 - `@vsilverthorne`
- 10/10 - `@skassabian`
- 10/25 - `@vsilverthorne`
- 11/10 - `@skassabian`
- 11/25 - `@vsilverthorne`
- 12/10 - `@skassabian`
- 12/25 - `@vsilverthorne`

#### Creating the newsletter in Marketo

A day or two before the issue due date, create the newsletter draft. It's easiest to clone the last newsletter in Marketo:

1. Go to Marketing Activities > Master Setup > Outreach > Newsletter & Security Release
1. Select the newsletter program template `YYYYMMDD_Newsletter Template`, right click and select `Clone`.
1. Clone to `A Campaign Folder`.
1. In the `Name` field enter the name following the newsletter template naming format `YYYYMMDD_Newsletter Name`.
1. In the `Folder` field select `Newsletter & Security Release`. You do not need to enter a description.
1. When it is finished cloning, you will need to drag and drop the new newsletter item into the appropriate subfolder (`Bi-weekly Newsletters`, `Monthly Newsletters` or `Quarterly Newsletters`).
1. Click the + symbol to the left of your new newsletter item and select `Newsletter`.
1. In the menu bar that appears along the top of your screen, select `Edit draft`.

#### Editing the newsletter in Marketo

1. Make sure you update the subject line.
1. Add your newsletter items by editing the existing boxes (double click to go into them). It's best to select the `HTML` button on the menu bar and edit the HTML so you don't inadvertently lose formatting.
1. Don't forget to update the dates in the UTM parameters of your links (including the banner at the top and all default items such as the "We're hiring" button).

#### Sending newsletter test/samples from Marketo

1. When you're ready, select `Email actions` from the menu at the top, then `Send sample` to preview.
1. Enter your email in the `Person` field, then in `Send to` you can add any other emails you'd like to send a preview too. We recommend sending a sample to the newsletter requestor (or rebecca@ from the content team for marketing newsletters) for final approval.
1. When you are satisfied with the newsletter, select `Approve and close` from the `Email actions` menu.

#### Sending the newsletter

1. When the edit view has closed, click on the main newsletter item in the left-hand column.
1. In the `Schedule` box, enter the send date and select `Recipient time zone` if the option is available.
1. Make sure `Head start` is checked too.
1. In the `Approval` box, click on `Approve program`.
1. Return to the newsletter issue and leave a comment telling requestor (@rebecca from the content team for marketing newsletters)  to double check all has been set up correctly. Close the issue when this is confirmed.
