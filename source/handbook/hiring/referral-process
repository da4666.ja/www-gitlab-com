---
layout: handbook-page-toc
title: "Referral Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##How to Refer a Candidate in GreenHouse

Details on how to refer a candidate in GreenHouse and how to request an update on the candidate are found [here](https://about.gitlab.com/handbook/hiring/greenhouse/#making-a-referral).

##How to Respond to Referral Update Requests

The Recruiting team utilizes [GitLab Service Desk](/product/service-desk/) to track incoming emails to the referral@ email. 

Under this [Referral Service Desk Project](https://gitlab.com/gl-recruiting/referrals) set up the proper notifications
   - Click on the bell icon on the top right next to Star and Clone
   - Go to Custom Settings
   - Check "New issue"
   - Closeout the window

1. On the left-side menu bar click Issues
   - This is where all our incoming referral@ emails will create an issue. You'll get an alert when someone sends an email to the referral@ email. Any "emails" that need to be addressed will be an open issue listed within this project.
1. Click on the new Issue
1. Assign it to yourself on the right-side toolbar
1. Read the Issue message
1. Add the appropriate label
1. Look up the candidate in GreenHouse if applicable.
1. Include the GreenHouse profile link in the comment.
1. Respond to the "email" by adding comments to the issue (be sure to enter comments as you would an email to team member asking for the update, they see all communication on the issues). Example responses:
   * Dear x, thank you for reaching out for an update on your referral. Per our [SLA](https://about.gitlab.com/handbook/hiring/greenhouse/#making-a-referral) please make sure you give us 10 days to review the submission. @recruiter's name is responsible for this role and will provide the candidate with an update soon. (insert GreenHouse link)
   * Thank you for the referral. @recruiter's name is responsible for this role and will provide you an update soon. (insert GreenHouse link)
1. Be sure to at mention the recruiter on the req so they are aware an update is being requested.
1. If this one comment address the entire message add a comment and close the issue. If the one comment does not address the entire message then only select comment.
1. The Recruiter would reassign the issue to themselves once they pickup the communication and add any applicable labels.
1. The assignee should close the issue when communication is complete.
