---
layout: handbook-page-toc
title: Support Modes of Work (and how to do them)
category: Handling tickets
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Modes of Active Support Work

At GitLab, we want to create the support team of the future. Remote is a great first step, but one of the biggest challenges in any support organization is accountability. How do we maintain accountability while also encouraging flexibility? We currently have four rotating roles so that people know what to do and have variation in their work:

1. [On call](/handbook/support/on-call/)
1. [First Response Time Hawk](#first-response-time-hawk)
1. [SLA Hawk](#sla-hawk)
1. [My tickets](#my-tickets)



## First Response Time Hawk
### What is a First Response Time Hawk?

This is a rotating role, that will have someone be "on point" weekly, to make sure:

1. new Premium SLA tickets get a first response as soon as possible
1. all new tickets are appropriately categorized and prioritized as they arrive

First Response Time Hawks directly drive achievement in our [Service Level Agreement KPI](/handbook/support/#service-level-agreement-sla).

### The Process in Action

1. Every Week on Fridays, Managers will add a First Response Hawk for each region to the Support Week in Review to announce who will be responsible that week.
1. If you are FRT Hawk, you are responsible for:
    1. Premium and Ultimate first replies for tickets created from 9am - 5pm in your time zone
       - **Note:** if you are a US Citizen and have access to the Federal instance, this should also be a part of your rotation.
    1. Triaging tickets to make sure they have:
      * An associated organization
      * [The correct priority](/handbook/support/workflows/setting_ticket_priority.html#setting-ticket-priority)
      * The correct form (such as GitLab.com, if they're asking for GitLab.com support)
      * The correct **Self-managed Problem Type** selected
1. [Keep an eye on the FRTH View in ZenDesk](https://gitlab.zendesk.com/agent/filters/360060245433)
1. You have full authority to call in and ask others for help if volume is high, or you are stumped.
1. You should expect to see new and different things that you are not an expert in. Where possible, take the time to dive deep and try and understand the problem.

### Things to be mindful of

1. Given that there's a FRT Hawk _per timezone_, your shift will overlap with another hawk.
    * This means you have a teammate within a team; yay! Be sure to communicate with your fellow hawk so you can help each other out in case one of you is stuck on a ticket, and also to make sure you're not
    both quietly working on the same ticket

1. Your focus should be new tickets in the Premium & Ultimate queue, especially high and medium priority. If there are no new tickets in that queue you can keep jump to the Starter & Free queue.

1. You might not cover all the tickets you wanted to cover on your shift, and that's okay
   * Just do your best 
   * Take a break if you need one - making sure your team knows you're stepping away by posting in `#support-team-chat`
   * Ask the rest of the team for help when needed in `#support_self-managed`

### Dashboards

You can track your progress and see global results on [FRT Zendesk Dashboard](https://gitlab.zendesk.com/agent/reporting/analytics/period:0/dashboard:ab7DLgKtesWr)

## SLA Hawk
### What is an SLA Hawk?
Like the First Reply Hawk, this is a rotating role the you will carry out for one week at a time (Monday to Friday). It is suggested that two people per region will carry out the role together each week. You'll work closely with your partner to divide work, escalate and formulate responses that will move tickets towards resolution.

SLA Hawks are responsible for taking action on the 'most breached' or 'next to breach' tickets to ensure that we're meeting SLAs and difficult tickets are not ignored.

SLA Hawks drive achievement in of our KPI of [Service Level Agreement KPI](/handbook/support/#service-level-agreement-sla).

If you are an SLA Hawk, you are responsible for:
    1. Ensuring that tickets do not breach their SLAs by issuing subsequent replies
    1. Proactively reaching out for help and escalating issues appropriately to ensure progress towards ticket resolution
    1. Resetting Priority on Tickets as per our [Definitions of Support Impact](/support/#definitions-of-support-impact) if the ticket narrative changes
    1. Verifying that tickets have the correct **Self-managed Problem Type** selected

### The Process in Action
1. Every Week on Fridays, Managers will add 2x SLA Hawks for each region to the Support Week in Review to announce who will be responsible that week.
1. Each day sort the Zendesk view for 'Self Managed Premium and Ultimate' by 'Next SLA Breach' `ascending`
1. Start at the top of the list
1. Try to get a reply out for the top ticket.
1. If you don't know how to respond or can't come up with any additional actions, link the ticket in Slack and ask for help from your team-mates.
1. If you don't get any timely help from colleagues, `@mention` your manager in Slack in a thread where you initially linked the ticket and ask them what the next steps should be
1. Go to the next ticket down the list and repeat
1. If there are no imminent breaches - repeat the process with the 'Starter and Free' view.

![SLA Hawk process flowchart](assets/sla-hawk-workflow.png)

## FAQ about SLA Hawk role

1. **Do we need to do the SLA Hawk role all day every day?** It's up to you - unlike being on call, there's no requirement to be in the queue all day. Use your best judgement and try to at least clear all breached tickets. If there are no imminent breachers then feel free to work on something else.
1. **How long should we spend trying to reply to a ticket while in SLA Hawk role?** Use your best judgement - use your best judgement - typically you might spend longer than normal since your are looking at the most 'stuck' tickets. If you're making progress, keep going until you get a reply out. If you're not getting anywhere by reading, searching and trying to raise help from others in Slack, feel free to go to the 'escalation' step and ping your manager with the ticket link in `#support_self-managed`.
1. **Can we ask someone else to reply to a ticket?** Absolutely! If a team-mate is online and already assigned to a ticket or has context about it, please ask them in `#support_self-managed` if they can work on the ticket now and free you up to move on.

**A key goal of this role is to get 'ignored' or 'wallowing' tickets moving. You should not move onto the next ticket until you have replied, asked for help and if necessary pinged your manager in Slack.**

### Things to be mindful of


1. Teamwork is key - there will be another person in your region in this role, so stay in communication with them throughout your day.
1. Not all next replies need to be technical - in some cases a ticket with an imminent breach may have an assignee who is offline *and* the ticket requestor indicated that they wanted replies in a region other than yours. In this case, a reply like the following may be appropriate:
 > It looks like John is handling this ticket in the US. He'll be back online in the morning with an analysis of what you've sent along. If you do happen to be online and would like one of us in APAC to take a look now, we're happy to do so - just reply here to let us know you're around and we'll get to it as soon as we're able.
1. If a ticket has an assignee (and they're online) it's okay to ask them what the status of the ticket is. If they're still working on getting an environment set up to reproduce the issue, indicating this to the customer is an appropriate next response. It is OK to submit the ticket as 'Open' - the person assigned to the ticket will still see it in their 'My Assigned Tickets' and will respond as soon as they can (see 'My Tickets' workflow below).
1. While you need not assign tickets to yourself in this role, you're welcome to - if you see one that looks interesting, grab it!

### Dashboards
Coming soon.

## My Tickets
### What is the "My Tickets" workflow?
If you don't have a specialized role this week, then you'll be working the "My Tickets" workflow. This is your 'normal' role - you're free to work on what makes most sense to you (including learning, writing up issues, improving docs etc).

The process here explains what to do when your actively working on tickets in Zendesk. You'll be moving issues along and acting as an escalation point for those in Hawk roles or Emergencies. Additionally, in this mode you'll be acting as a steward of longer running issues - making sure that a single person has the context necessary to drive things to resolution.

**By assigning a ticket to yourself, you become the current 'custodian' for the ticket. The whole team will still help and reply to all tickets, so you can expect others to step in and add replies. The reason for taking assignment is to help ensure tickets don't get ignored or stuck. If you have an assigned ticket that's stuck, follow the escalation process described below to get it moving along. You don't need to know how to solve the ticket because it's assigned to you.**

If you're working in "My Tickets" you're driving achievement in of our KPI of [Support Satisfaction](/handbook/support/#support-satisfaction-ssat).

### The Process in Action
1. At the start of the day, go to 'My assigned tickets' view and start at the top
1. Try to get a reply out for the top ticket.
1. If you don't know how to respond or can't come up with any additional actions, link the ticket in Slack and ask for help from your team-mates.
1. You can reassign a ticket to another colleague at any time
1. If you don't get any timely help from colleagues, `@mention` your manager in Slack in a thread where you initially linked the ticket and ask them what the next steps should be
1. Go to the next ticket down the list and repeat
1. If you run out of Assigned open tickets, work in the queues as normal by finding a ticket you're able to help with - write a response and assign it to yourself.

![My Tickets process flowchart](assets/my-tickets-workflow.png)

### Things to be mindful of
1. Teamwork is no less important with this workflow; be aware of what's happening across the board and jump in!
1. Assigning a ticket to yourself doesn't mean that you have to know how to solve it; it just means you have to keep the context of the ticket in your mind and help move things forward. If you're unable to do so, you can re-assign tickets to a colleague (with their permission, of course) or escalate to your manager by pinging them in Slack in `#support_self-managed` with a link to the ticket.

### Dashboards
Coming soon.

## Mermaid Source

#### SLA Hawk

~~~

   ```mermaid

   graph TD
   A[Sort 'Premium and Ultimate view by <br> 'Next SLA Breach'] -->
   B(Open the <br> top ticket)
   B --> C{Can you reply <br> to the ticket?}
   C -->|Yes| D[Send <br> reply] --> H
   C -->|No| E[Ask for help in Slack <br> and link the ticket]
   E -->|Help received| F[Send <br> reply] --> H
   E -->|No help recieved| G[Mention your manager in Slack <br> on the ticket thread] --> H[Go to the next ticket <br> - if no imminent breaches - <br> go to 'Starter and Free' view] --> B
   ```

~~~

#### My Tickets

~~~

   ```mermaid

   graph TD
   A[Open the 'My Assigned Tickets View'] -->
   B(Open the top ticket)
   B --> C{Can you reply to the ticket?}
   C -->|Yes| D[Send reply] --> H
   C -->|No| E[Ask for help in Slack and link the ticket]
   E -->|Help received| F[Send reply] --> H
   E -->|No help recieved| G[Mention your manager in Slack on the ticket thread] --> H[Any tickets left in 'My Assigned Tickets' view?]
   H --> |Yes| I[Repeat this process] --> B
   H --> |No| J[Open the main views and find a ticket to work on]
   --> K(Send a reply and assign to yourself) --> J

   ```
~~~
