---
layout: markdown_page
title: "FY20-Q4 OKRs"
---

This fiscal quarter will run from November 1, 2019 to January 31, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO: IACV
1. CRO: Build strong FY21 pipeline. FY21-Q1 pipeline at 2X target (stage 3+), FY21-Q2 pipeline at 1.2X target (stage 3+)
    1. VP Field Ops: Instrument systems and programs to intended targets.  Factor pipeine targets to relevant tools,  target 90%+ completion of certification programs.
        1. Sr Director of Sales Operations: Assess solution for pipeline targets.  Implement targets into Clari (or SFDC),  measure effectiveness to 90% accuracy
        1. Director of Sales Enablement: Develop certification ROE and processes.  Verify Managers have scheduled review, target 90%+ completion of certification programs.
    1. VP Alliances: Broaden addressable user base through user personas (OPS and Security). AWS Re:invent GitLab visibility (5 sessions), Kubecon 14 Top K8s partners with GitLab demos for 400 customers each, launch partner marketing campaign.
    1. VP Alliances: Deliver GitLab as part of our partner’s sales processes. Support building channel with new VP, double lead flow from key alliance partners, deliver first GitLab session at Dreamforce
    1. VP Alliances: Build tighter partner integrations with GitLab for K8s, Marketplace and Authentication. Grow secure stage through partnerships, create 1st class experiences for core partner compute services, establish shared technical roadmap with Hashicorp.
1. CRO: Ensure public company readiness. Sales & Marketing using a single pipeline dashboard, Conduct mock earnings call\
    1. VP Field Ops: Align GTM goals and optimize reporting cadence to support IPO. Create SSOT dashboard of key GTM KPIs, target <1day EOM close
        1. Sr. Manager of Analytics: Partner with GTM Sales/Marketing Metrics Team to create SSOT dashboard view of GTM KPIs. Launch Periscope dashboard, highlight 3 KPIs of focus for GTM Motion Team.
        1. Sr Director of Sales Operations: Optimize EOM close.  Provide final bookings <1 day by simplification of IACV calc, automation >50% of EOM close processes, document new processes in Handbook
    1. Customer Success: Decide on customer success platform. Define requirements, Assess vendors, Contract signed.
    1. Customer Success: Restructure handbook to align GitLab organization to the customer journey. Information architecture created with 100% of content relating to customer journey. 50% of handbook content restructured to new design as measured by group's content.
        1. TAM: Complete onboarding processes. Segment-specific processes (enterprise, midmarket, SMB), 2 digital content assets, survey created and sent to all 100% of Enterprise customers post-creation.
        1. TAM: Improve engagement approaches for key processes (EBR, TAM account reviews, GitLab days). Processes for each 3, 2 Templates (EBR and reviews), Enablement for 90% of TAMs.
        1. TAM Comm: Develop scalable onboarding approach. 2 webinas, 1 Email campaigns, Updated TAM process in handbook.
        1. CS Ops: Implement time-to-value reporting for 3 metrics. Time to engage, onboarding complete, and first value delivery, Reporting delivered for 100% Enterprise and midmarket custoemers.
        1. Comm SA: Define systematic Commercial presales technical buyers journey. Formalized tech eval process for 3 segments Tier 1 MM, MM, SMB), Tech eval and handoff processes updated.
        1. SA West: Establish Enterprise Large presales tech eval process. Identify success patterns, Improved activity tracking requirements created for SFDC, 2 SA playbooks.
        1. PS Solutions: Develop plans for future operational and strategic service offerings. Revenue model created with finance, Customer survey and feedback reports created for 100% of customers (Q3 and Q4), New service offering plan created for 1 year outlook.
        1. PS Solutions: Develop services onboarding for 2 partners for training capacity. Training plan, Management plan, 2 partners MSAs signed.
        1. PS Solutions: Complete end-to-end process process to streamline sales process. End-to-end process streamlined, Control models updated, 100% of process steps and approvals documented.
        1. PS Delivery: Improve repeatability and quality of service delivery. Close backlog older than 120 days, Scheduling and resource management, Expectations management training delivered to 100% of PSEs.
        1. PS Education: Develop modular "GitLab Total Product Training." Update existing training, Develop content for 1 complete product training.
        1. VP Field Ops: Align GTM goals and optimize reporting cadence to support IPO. Create SSOT dashboard of key GTM KPIs, target <1day EOM close
            1. Sr. Manager of Analytics: Partner with GTM Sales/Marketing Metrics Team to create SSOT dashboard view of GTM KPIs. Launch Periscope dashboard, highlight 3 KPIs of focus for GTM Motion Team.
            1. Sr Director of Sales Operations: Optimize EOM close.  Provide final bookings <1 day by simplification of IACV calc, automation >50% of EOM close processes, document new processes in Handbook
1. VP of Engineering: [Enterprise-grade GitLab.com](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5566)
    1. Sr Director of Development
        1. Director of Engineering, Enablement
            1. [Iteratively re-architect our queue implementation to create 10x headroom](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4951)
            1. [Meet enterprise customer expectations around timely feature delivery](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5569)
        1. Interim Director of Engineering, CI/CD
            1. [Get CI queue times (p95) under 1 min](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5341)
            1. [Meet enterprise customer expectations around timely feature delivery](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5605)
        1. Director of Engineering, Dev: [Meet enterprise customer expectations around timely feature delivery](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5686)
        1. Interim Director of Engineering, Ops: [Meet enterprise customer expectations around timely feature delivery](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5612)
        1. Director of Engineering, Defend: [Meet enterprise customer expectations around timely feature delivery](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5626)
        1. Director of Engineering, Secure: [Meet enterprise customer expectations around timely feature delivery](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5611)
        1. Director of Engineering, Growth
            1. [Meet enterprise customer expectations around timely feature delivery](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5687)
            1. [Growth stage: Perform weekly iterations](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5692)
    1. Director of Infrastructure: [Drive GitLab.com Availability to 99.95%](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5623)
    1. Director of Quality: [Improve GitLab Enterprise features robustness by improving functional and performance test coverage](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5607)
    1. Interim Director of Security: [Improve GitLab Security resilience and availability by creating independent GitLab Security environments](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5660)
    1. Director of Support: [Create sustainable process to achieve 85% performance to Next Response Time SLA](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5589)
1. CMO: Cost-effective IACV pipeline growth
    1. Community: Showcase activity generated by GitLab Heroes. 10 GitLab use-case videos or blogs published, 5 GitLab talks given by Heroes.
    1. Corp Mkt: Increase in awareness around GitLab CI/CD in response to competitor GA announcement. Be included in 25% of the articles about competitor announcement, drive 1000 form fills on the competitive campaign landing page.
    1. Corp Mkt: Successful pull-through of multi-cloud message at KubeCon. Reach 5,000 people directly (50% of attendees) via KubeCon talks or 0-day events, be included in 5 multi-cloud focused articles and partnerships highlighted in 10 event articles, 500,000 social media impressions from tanuki adventure.
    1. Corp Mkt: In-quarter demonstrable results from AWS re:Invent. Collect 5,000 leads from AWS re:Invent, 500 follow-up requests, 50 SAOs and $1m in pipeline by end of Q4 (a 2-1 pipe to spend by end of Q).
    1. Corp Mkt: Increase awareness of GitLab in public relations efforts. 50% SOV vs. main competitor, company mentioned in 30 business press articles, and at least two key messages included in 75% of feature articles.
    1. Revenue Mkt: Optimize value-driver campaigns. Decrease cost-per-lead to $230 by end of Q4, increase MQL to SDR-QL conversion rate by 20%.
    1. Strategic Mkt: Incorporate AWS, Google Cloud & VMWare partnership assets into 3 value-driver campaigns. One partner-specific content asset in each PF track, 3 partner demand-gen activities that drive to value driver campaign, 1 technical blog for each partner introduced in each campaign.
1. CFO: Improve financial planning processes to support growth and increased transparency.
    1. Finance: [Single source of truth for hiring process designed and implemented](https://gitlab.com/gitlab-com/finance/issues/629)
    1. Finance: [Complete financial model from top of funnel through closed won](https://gitlab.com/gitlab-com/finance/issues/1233)
    1. Finance: [Version control for financial model that allows for real time analysis without impacting production model](https://gitlab.com/gitlab-com/finance/issues/964)
    1. Finance: [Automate 100% of Investor Metrics in Periscope](https://gitlab.com/gitlab-com/finance/issues/594)

### 2. CEO: Popular next generation product

1. VP of Product Strategy: Get strategic thinking into the org. Secure [section strategy review](https://gitlab.com/gitlab-com/Product/issues/381), produce [strategy visuals](https://gitlab.com/gitlab-com/Product/issues/512).
1. VP of Product Strategy: Get acquisitions into shape; build a well-oiled machine. Build [soft-landing identification software](https://gitlab.com/gitlab-com/corporate-development/issues/1), identify 1000 [qualified acquisition targets](/handbook/acquisitions/performance-indicators/#qualified-acquisition-targets).
1. VP of Product Strategy: Lay groundwork for strategic initiatives. Research 5 [strategic initiatives](https://docs.google.com/document/d/1CBvC5iSgvSfn0oBAu3ph9MAzrMHxhKCSjvigz0Lm1VM/edit).
1. VP of Product Management: Deliver on product roadmap.  Achieve 100% of our [category maturity](/direction/maturity/) plans.
1. VP of Product Management: Proactively validate problems and solutions with customers.  At least 2 [validation cycles](/handbook/product-development-flow/#validation-track) completed per Product Manager.
1. VP of Product Management: Create stage level product demos to build customer empathy.  Deliver one recorded demo for each stage.
1. Director of Product, Growth: Establish a rapid cadence of testing & learning.  At least one deliverable per week, per growth group.
1. Director of Product, Growth: Improve the customer experience with billing related workflows.  Make material improvements to direct signup, trial, seat true-up, upgrade, and renewal workflows, both for .com and self-hosted customers, driving at least $570k in incremental ACV (IACV). 
1. Director of Product, Growth: Improve the quality of product usage data. Deliver accurate [SMAU] (/handbook/product/metrics/#adoption) dashboards for each stage, including both .com and self-hosted customer usage data.
1. Director of Product, Secure & Defend; Group Manager, Enablement: Communicate updated product visions at the Section level to inform product investment decisions. Deliver product vision content for Secure & Defend and Enablement sections by end of Q4.
1. VP of Engineering: [Raise productivity thru iteration without jeopardizing availability](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5564)
    1. Sr Director of Development: [Deliver on our product vision by being more iterative](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5648)
    1. Director of Infrastructure: [Achieve daily deployment velocity](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5630).
    1. Director of Quality: [Increase Engineering efficiency and productivity by speeding up test and pipeline duration](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5608)
    1. Interim Director of Security: [Improve the GitLab product by dogfooding Secure & Defend features department-wide](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5661)
    1. Director of UX: [Make the GitLab UI more beautiful by burning down remaining UI Beautification issues.](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5573)
    1. Director of UX: [Help make the GitLab product more loveable by sharing UX work broadly and frequently to reinforce our overall vision.](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5571)
1. CMO: Building scalable future capabilities
    1. Community: Grow Heroes program, with a Q4 focus in North America. Establish Heroes governing council, 5 new meetup groups in NORAM, 10 new Heroes from NORAM.
    1. Community: Pilot GitLab “Hackathon-in-a-box.” Develop packaged Hackathon offering to facilitate contributions at face-to-face events, pilot offering at 3 events in Q4.
    1. Revenue Mkt: ABM programs running. Demandbase implemented, target accounts identified by territory, ABM metrics baseline established, territory campaigns running in 2 territories.
    1. Strategic Mkt: Roll out GitLab ROI framework. Deliver ROI business justification tool for sales, Work with Forrester to complete first customer data collection for Total Economic Impact report.
    1. Strategic Mkt: Introduce use cases as framework for GTM.  Roll out first 4 use cases, align use cases with 3 content pillars, publish a web page dedicated to each use case, publish demo for for each use case.
    1. Strategic Mkt: Reset competitor and DevOps tools comparison web strategy. Implement 5 loveable competitive comparison page revamps and Crayon cards, establish simplified comparison page format for ‘functionally similar’ DevOps tools.
    1. Strategic Mkt: Pilot Learn@gitlab web experience for finding & featuring how-to technical content. Publish MVC landing page or PF experience, tag existing content into new page, build plan for FY21.

### 3. CEO: Great team

1. VP Recruiting: Deliver on the ambitious hiring plan, in partnership with leaders, while ensuring a unique and exceptional candidate experience and that candidate pools are diverse and representative of our communities. At least 90% of plan for the quarter. Average time to fill at 40 days, excluding Director+. ??% of hiring manager interviews are with females candidates.
1. Chief People Officer: Support an environment where all team members feel they belong, are valued, and they can contribute; understand the key drivers for team member engagement and continuously improve. Select 1-2 top priorities from October 2019 Engagement Survey and develop action plan. Training modules rolled out: at least 2 D&I training modules + communication/management training. ??% of hiring manager interviews for leadership roles are with with female candidates.
1. Chief People Officer: Improve onboarding to ensure optimal levels of new team member experience and productivity. OSAT (Onboarding Satisfaction): Increase response rate and maintain score of 4.0 or higher. ROI calculation for new HRIS that meets the needs of a medium to large size company. 100% of People Ops team trained to effectively handle team member questions.
1. Chief People Officer: Team members understand the value of their compensation, benefits, and perks. Annual comp review with updated location factor based on data from ERI, Numbeo, Compensation Surveys while taking the Average Location Factor KPI into account. Measure and ensure gender pay equity. Ensure competitive benefits offerings where we have entities/PEOs.
1. CRO: Implement an efficient and effective hiring model. Talent model used end to end on at least 5 hires in Q4, Manager Span of Control (equal to or less than 8) for entire CRO org.
    1. VP Field Ops:  Partner with VP Recruiting to refactor hiring process leveraging correct hiring targets.  Forecasting hiring pipleine >100% of targets, Partner with PeopleOps to use Talent Model >4 hires, target >80% of MSoC < 8 FTEs
    1. Customer Success SA West: Improve presales technical account planning. Assessment of successful/unsuccessful approaches, Complete updated tech evaluation process integrating CoM in handbook.
    1. Customer Success Ops/Enablement: Develop CoM playbooks / cards for key TAM expansion plays. SCM to CI, Security, Starter to Premium, Premium to Ultimate playbooks documented (Note: sales plays may not be in handbook).
    1. Customer Success Ops/Enablement: Enhance onboarding for Customer Success team. Updated training delivered to 80% of team, Enablement guide, New employee survey delivered to 100% of onboarded team members (once completed).
    1. Customer Success Ops/Enablement: Launch peer to peer continued education program. Library of recorded training sessions 3 digital assets, Quick response training, 2 small consumable training sessions delivered.
    1. Customer Success East: Develop SA experts program. Establish 3 stage experts, Define role and responsibilities in handbook.
1. VP of Engineering: [Provide mentorship/coaching at every level](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5565)
    1. Sr Director of Development
        1. Interim Director of Engineering, CI/CD: [Maintain current developers to maintainers ratio](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5605)
        1. Director of Engineering, Dev: [Maintain current developers to maintainers ratio](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5688)
        1. Interim Director of Engineering, Ops: [Maintain current developers to maintainers ratio](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5612)
        1. Director of Engineering, Defend: [Maintain current developers to maintainers ratio](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5624)
        1. Director of Engineering, Enablement: [Maintain current developers to maintainers ratio](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5569)
        1. Director of Engineering, Secure: [Maintain current developers to maintainers ratio](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5611)
        1. Director of Engineering, Growth: [Maintain current developers to maintainers ratio](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5689)
    1. Director of Infrastructure: [Hire key Infrastructure Management vacancies](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5631)
    1. Director of Quality: [Scale Quality Engineering to all product stages by growing to plan](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5609)
    1. Interim Director of Security: [Develop FY21 Security strategy and organisational structure](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5662)
    1. Director of Support: [Develop FY21 Support strategy and supporting organizational direction](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5591)
    1. Director of Support: [Rebase interlock with Customer Success](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5592)[Rebase interlock with Customer Success](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5592)
    1. Director of UX: [Scale UX Research by building out Research Training materials that Product Managers and Product Designers can use to consistently deliver results.](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5572)
1. CFO: Issue Financial Statements and Improve Accounting Processes:
    1. Controller: Achieve 12 day close by Q4 FY 2020, and 10 day close by Q2 FY 2021.
    1. Principal Accounting Officer: No material adjustments in audit; no material post-close adjustments in monthly close
    1. Accounts Payable: Complete Tipalti implementation
    1. Payroll: HR data integration between Bamboo/ADP; Update handbook with a payroll section (Shared OKR with PeopleOps)
    1. Payroll:  Proposal to replace Expensify
    1. PAO/Revenue: Complete retrospective review of Revenue Allocation.
    1. Controller: Propose vendor and implementation plan for rev rec (formerly RevPro) and Commissions (Xactly).
    1. PAO: No material weaknesses identified by Audit Firm.
1. CFO: Build a scalable data infrastructure that ensures completeness of data, quality and ability for anyone in the organization to access data and perform analysis.
    1. Data Team: Embedded/Central analyst/engineer model implemented so each function has at least one embedded data analyst.
    1. Data Team: 66% of Sensitive data is identified at the database object level (columns, tables, etc.) in a file in our repository for use in automated modeling, administering, and auditing of the warehouse.
    1. Data Team: 66% of data sources of Data quality measures and tests documented and implemented for data sources.
1. CFO: Create an IT organization that prepares the company for growth over next 18 months.
    1. IT: Create an IT SLA framework and issue initial target of less than three days.
    1. IT: 100% of onboard/offboard activities included in new hire/termination issues with automated notification to system DRIs.
    1. IT: Complete audit of system access termination for offboarded team members.
1. CFO: Create scalable business systems with business processes that have cross functional ownership.
    1. Business Systems: Technology Stack and DRIs in handbook in relevant places in handbook (avoid directory problem).
    1. Business Systems: Defined technology approach for back end of portal.
1. CFO: Improve purchasing function to streamline process while saving money.
    1. Procurement: Public RFP issued to replace Zuora as Gitlab's Subscription Management and CPQ solution.
    1. Procurement: Streamlined workflow based on procurement type. Involve purchasing early in the process.
    1. Procurement: Deliver quantified savings of > $250,000 in Q4.
1. CMO: Increase marketing team operational efficiency
    1. MktOps: Improve SDR lead processing efficiency. Reduce lead views to 5 (from 30+ today), average lead processing time for SDR under 5 minutes (from 10+ minutes), MQL lead and contact views emptied every day.
    1. MktOps: Less manual and more accurate lead routing. DemandBase form append running on all inbound forms, reduce manual lead routing by 50% (from 4k per month), no cutting-and-pasting data from DataFox/Discover.org.
    1. MktOps: Automate lead followup. Lead status in Outreach synced to SFDC lead status, ability to auto-sequence low-scoring leads launched, Drift bot qualifying and routing website leads.
    1. Community: Increase response coverage and improve responsiveness on high priority channels. Decrease median first reply time to 5 hours for Hacker News and website/blog comments, add YouTube GitLab channels and Linkedin to ZenDesk integration and response workflow.
    1. Strategic Mkt: Establish PIs (performance indicator) for each Strategic Marketing team. Each sub-team at least one PI trackable in a Periscope dashboard, point-of-reference tool implemented, AR coverage “heatmap at-a-glance” implemented.
    1. Revenue Mkt: Implement organic search optimizations. Establish catalog of SEO terms, identify first list of SEO optimizations, optimize 10 website pages for key terms.

## How to Achieve Presentations

* [Engineering](https://www.youtube.com/watch?v=MW_CfE5-vG4)
* [Finance](https://www.youtube.com/watch?v=bLW_fosy07g&feature=youtu.be)
* [Marketing](https://www.youtube.com/watch?v=3PN-0kTMyjA&feature=youtu.be)
* [Product](https://www.youtube.com/watch?v=RdPbpO-dquM)
* [Product Strategy](https://www.youtube.com/watch?v=p4-X2RCzCQo)
* Sales
* [People](https://www.youtube.com/watch?v=sVGRNcK6O5s&feature=youtu.be)
